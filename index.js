// 1-task
const vowels = ["a", "e", "i", "o", "u"]

function countVowel(str) {
    let count = 0;

    for (let letter of str.toLowerCase()) {
        if (vowels.includes(letter)) {
            count++;
        }
    }
    return count
}
const string = countVowel("The quick brown fox")

console.log(string);

//2-task
var newYear = new Date('Jan 1, 2022 00:00:00').getTime();

function counter(){
    let dateCurrent =  new Date().getTime(),
        timeLeft = newYear - dateCurrent;
     let second = 1000,
        minute  = second * 60,
        hour    = minute * 60,
        day     = hour * 24;
    let dayLeft     =  addZero(Math.floor(timeLeft/ day), 3),
        hourLeft    =  addZero(Math.floor((timeLeft % day)/ hour), 2),
        mintueLeft  =  addZero(Math.floor((timeLeft % hour)/minute), 2),
        secondLeft  =  addZero(Math.floor((timeLeft % minute)/ second), 2);


    function addZero (num, count) {
        return num.toString().padStart(count, "0");
    }       

    console.log(dayLeft + ":" +hourLeft+ ":" +mintueLeft+ ":" +secondLeft)

}

setInterval(() => {
    counter();
}, 1000)

//3-task
function findSymbol(arr){
    const symbols = /[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/@#]/g
    let result = [];  
    for(let i=0; i<arr.length; i++){
        let check;        
        (!(check = arr[i].search(symbols))) ? result.push(arr[i]) : 'There is no such symbols'
    }
    return result   
}
arr=['S','@','!','o','m']

console.log(findSymbol(arr))

//4-task
function getIntersection(arr1, arr2){
    var result = [];
    arr1.forEach(function(elem){
        arr2.forEach(function(elem2){
            if(elem === elem2){
                result.push(elem);
            }
        });
    });
    return result;
}

console.log(getIntersection([1,2,3], [2,3,4,5]));

//5-task
let msg = "Hello Udevs"
setTimeout (()=>{
alert(msg)
},4000)

//6-task

const sumNaturalnum = () =>{
    let arr = [1,2,3,3.1,4.2,5,6,7.1]
    let sum =new Array()
    let reducer = (a,b) => a+b;
    for(let i = 0; i<arr.length; i++){    
        (Number.isInteger(arr[i])) ? sum.push(arr[i]) : 'There is no natural number';}
    return sum.reduce(reducer)
}
    
console.log(sumNaturalnum())

//7-task
function countString(str, letter) {
    let count = 0;
    for (let i = 0; i < str.length; i++) {
        if (str.charAt(i) == letter) {
            count += 1;
        }
    }
    return count;
}

console.log(countString('there was message which had sent by my side','y'));